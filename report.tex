% BEGIN PREAMBLE
%##############################################
\documentclass[12pt]{report}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{float}
\title{State Space Analysis of Two-phase Commit Protocol}
\author{Marius Fjeld Wold}
%##############################################
% END PREAMLBE

\begin{document}
\maketitle

\paragraph{Introduction}
\subparagraph{}
The CPN model to be analysed will be a model of a simple two-phase commit protocol that was developed as part of the first assignment for the MOD350 course. The tools provided by CPN-Tools will be used to examine the properties the model in order to determine the correctness of the model.

\paragraph{Model}
\subparagraph{}
The first property we are interested in is the size of the models state space. If the state space of the model is infinite no further analysis would have to be done as it would be impossible to verify the correctness of the model. This would be due to the fact that even though the section of the state space being examined indicates that the model is correct, it would not guarantee that it would remain so given a slightly larger section of the state space, and so on.

\subparagraph{}
If the state space is finite then we can be certain that we can indeed analyse and interpret every state that the modelled system could ever reach. Even if the state space is extremely large, causing the analysis process to be lengthy, its properties can still be examined and the model can be verified. 

\subparagraph{}
The initial version of the model, seen in figure \ref{InitialModel}, on which the analysis is based is not finite as the coordinator has no condition where it will cease polling the workers. This leads to the state space being infinite. In order make the state space of the model finite it was altered slightly. In the altered version the coordinator will send a 'can commit' message to all the workers at most three times before aborting and reporting that the process has failed. Should the coordinator receive a 'yes' vote from all the workers it will cease polling and report success.
Figure \ref{ModifiedModel} shows the section that was modified in order for the model to have a finite state space.
\begin{figure}[H]
	\includegraphics[scale=0.6, width=\linewidth]{Images/InitialNet}
	\caption{A section of the intial petri net}
	\label{InitialModel}
\end{figure}
\begin{figure}[H]
	\includegraphics[scale=0.4, width=\linewidth]{Images/ModifiedNet}
	\caption{The modified section of the initial petri net}
	\label{ModifiedModel}
\end{figure}

\subparagraph{}
Since we can now assume that the state space is finite we can use the tools provided by CPN-Tools to generate the state space report for the model. Since we can only assume that the model now has a finite state space it should be one of the first properties to be verified. Using the information presented in the state space report it is possible to verify whether or not the model has a finite state space.

\subparagraph{}
To get an indication as to whether or not the state space is now finite, we can examine number of nodes and arcs in the state space graph and the strongly connected component graph. We se that the number of nodes and arcs is the same for both, indicating that there are no infinite occurrence sequences. The absence of such sequences guarantees us that our protocol will always terminate and that it will not enter into an infinite loop due to the lack of cycles in the scc.

\paragraph{State Space Model}
\subparagraph{}
CPN-Tools also provides tools for visualising the state space of the model, this lets us visually verify properties that we expect the model to have and check its behaviour. The visual representations is composed of two types of components in CPN-Tools, nodes and arcs. Nodes represents the markings, or state, of the places in the model at a given point in an execution whilst arcs represents the transitions between these states. It is possible to visualise the entire state space with these tools, however, for non-trivial models, the state space has a tendency to be very large. This makes the diagrams difficult to read and interpret. A more manageable approach is to focus states representing key behaviour/functionality.

\subparagraph{}
One such state is the initial marking of the model and the the transitions that can occur from it. In Figure \ref{InitialStateSpace}, node one represents the initial marking of the model before any actions have been carried out. The numbers underneath the node tells us that it as zero predecessors, as expected, and one successor. Node number two has six possible transitions which are enabled. The nodes connected by the transitions represent all the reachable markings from it. Each reachable marking represent a possible vote by a worker, i.e marking six represents worker one submitting a 'yes' vote and having its token moved to the 'Prepared' place. As there is no defined order in which the workers votes there are six arcs, as each worker can be the first to respond. The two possible choices, yes and no, means there are six enabled transitions in total.
\begin{figure}[H]
	\includegraphics[scale=0.6, width=\linewidth]{Images/InitialStateSpace.png}
	\caption{Section of initial state space}
	\label{InitialStateSpace}
\end{figure}

\subparagraph{}
The previous diagram highlights a particular property of the protocol as it is currently modelled. As pointed out at the end of the last paragraph each worker can be the first to respond, there is now particular order in which the workers vote. This indicates that each worker operates independently of the other workers, if there are any, highlighting the decoupled nature of the workers.

\subparagraph{}
Another diagram depicting the workings of the model is seen in figure \ref{AllYes}. This diagram represents the marking in which all workers have voted yes on the first attempt. The predecessor of marking 54 is marking 36, here all the workers have voted yes leading to only one transition being enabled. Marking 54 represents the coordinator sending a 'commit' message to all the workers. There are three possible markings, each representing different workers being the first to act on the message.
\begin{figure}[H]
	\includegraphics[scale=0.6, width=\linewidth]{Images/AllYesFirstTry.png}
	\caption{All the workers voting yes on the first attempt}
	\label{AllYes}
\end{figure}

\subparagraph{}
Using the tools we can also examine the markings representing the coordinator making the decision to either commit or abort. An example of one such marking is seen in figure \ref{DecisionMaking} where only two workers have voted to commit with the last worker voting no. As seen in the diagram there is only one way in which the particular marking represented by node 139 can be reached which is through the transition from node 138. Whilst it is possible that the same voting pattern will occur in a later attempt the marking will differ due to the fact that the number of attempts will differ. Again we see evidence that the workers operate independently of one another, as represented by the two markings on node 141 and 140. These markings show that both workers can be the first to receive the decision to abort.
\begin{figure}[H]
	\includegraphics[scale=0.5, width=\linewidth]{Images/DecisionMaking}
	\caption{Coordinator decides to abort}
	\label{DecisionMaking}
\end{figure}

\subparagraph{}
The nodes that are identified as dead markings represent the terminal states of the model. In the report the markings represented by the nodes 180, 306, 307 and 378 are dead. These four markings represent the termination states of the model. A section of the diagram of these markings is shown in figure \ref{DeadMarkings}. By examining the markings and their predecessors it becomes evident that these markings represent the three successful termination states, taking one, two and three attempts respectively, and the one state in which the commit fails. The report also tells us that there are no dead transitions, which is to say, that there are no transitions will never be fired. Since there exists several dead markings in the state space it follows that there are no live transitions, these are transitions which can occur in a sequence from any reachable marking.
\begin{figure}[H]
	\includegraphics[scale=0.5, width=\linewidth]{Images/DeadMarkings}
	\caption{Dead markings and incoming transitions}
	\label{DeadMarkings}
\end{figure}

\paragraph{State Space Report}
\subparagraph{}
By analysing the content of the state space report we can verify properties that we believe the model to posses, a section of the state space report generated by CPN-Tools is shown in figure \ref{StateSpaceReport}. As mentioned in an earlier paragraph the number of nodes and arcs in both the state space and the strongly connected component(ssc) graph is calculated. As both the state space and the ssc have the same number of nodes and arcs the state space of the model is likely to be finite, as this indicates that there are no cycles present in the ssc.

\subparagraph{}
The report also gives values for the best integer bounds associated with each place, the values represent upper and lower bounds on the number of tokens that can be at a given place. The upper bound of three for the place 'Prepared' indicates the maximum number of tokens that can occur at the place. As the current model represents three workers, this makes sense as we cannot have more workers prepared to commit than we currently have in our model. The lower bound tells us that it is possible for the place to have no tokens on it during simulation, again this makes sense this can occur when none of the workers vote 'yes'. The same arguments hold true for the other places such as Decision, with the bounds denoting that the coordinators decision is sent to at most three workers and at least zero.

\subparagraph{}
The multi-set bounds property represents the possible multi-sets that can be present at a place. The value for the place Ack represents possible multi-sets along with an upper bound on the maximum number of appearances of each multi-set. For the place Decision there can be three occurrences of the Commit token and two occurrences of the Abort token. This is expected behaviour of the model as a commit decision will have to be sent to each worker whilst an abort message will at most have to be sent to two workers. One worker will have voted no and will not have a message sent to it by the coordinator.

\subparagraph{}
The lower multi-set bound is the opposite. It denotes the smallest possible multi-sets that will be present at a place during execution. The report tell us that all places have the empty set as the lowest possible multi-set that can occur.

\subparagraph{}
The report states that the state space does not have any home markings. A home marking is a marking which can be reached from all reachable markings in the state space. A home marking only exists if there exists one terminal scc. The lack of home marking is most likely due to the fact that the simulation can terminate in one of two possible states, failure or success. If the coordinator has made three attempts and the final attempt fails it is impossible to reach the Success place. The same holds true for the Failure place when the workers successfully commit. It is therefore expected for the model to not have any home markings, if a new idle place for the coordinator was added this could lead to the state space having a home marking.

\subparagraph{}
The dead markings represent the terminations states of the protocol and are markings from which there are no outgoing transitions. The dead markings have been discussed in a previous paragraph so the information will not be reiterated here. By examining the markings specified in the report we can verify that our model terminates the way we expect it to and ensure that there are no undiscovered ways in which it can terminate.

\subparagraph{}
There are no dead transitions reported meaning that all transitions in the state space have the chance to occur once. It does not guarantee that each transition will occur at least once only that it has the possibility to do so. If a transition is dead it means that there exists no marking in the state space for which the transition will be enabled.
\begin{figure}[H]
	\includegraphics[scale=0.5]{Images/ReportSection}
	\caption{Section of the generated state space report}
	\label{StateSpaceReport}
\end{figure}

\subparagraph{State space explosion}
State space explosion is a phenomena that can occur as models grow in complexity, as the model grows then the number of nodes in the state space will increase exponentially. State space explosion can also happen when additional tokens are added to the model, the increased number of tokens leading to an increase in the number of possible markings. This can lead to problems when performing analysis as more and more time will be required in order to investigate the state space. By simply increasing the number of workers it is possible to examine if state space explosion will occur in the current model. Generating the state space of the model with five worker tokens gives a significant increase in the number of nodes and arcs, in addition only a partial state space for the model has been generated. The results were the same with eight workers due to the limit on the number of nodes and arcs to calculate before terminating that CPN-Tools has.  The first part of the report is shown in figure \ref{StateSpaceReportFiveWorkers}.
\begin{figure}[H]S
	\includegraphics[scale=1]{Images/StateSpaceReportFiveWorkers}
	\caption{State space report with five workers}
	\label{StateSpaceReportFiveWorkers}
\end{figure}

\subparagraph{}
For the sake of curiosity an attempt was made to generate a state space of the model with eight workers whilst increasing the limit of both number of nodes and arcs to compute before terminating. On an above average powerful computer the calculation took in excess of 10 minutes to finish. The state space was still partial so it is contains more nodes and arcs than what is given in the state space report. The first part of the report can be seen in figure \ref{StateSpaceReportEightWorkers}
\begin{figure}[H]
	\includegraphics[scale=1]{Images/StateSpaceReportEightWorkers}
	\caption{State space report with five workers}
	\label{StateSpaceReportEightWorkers}
\end{figure}

\paragraph{Conclusion}
\subparagraph{}
Based on the analysis carried out the model of the protocol appears to be correct. It behaves as expected and we have a guarantee that it will terminate in one of the states that have been discussed. We cannot make any guarantees that the protocol will always succeed only that it will reach one of the terminal states. We also seen that we can with, relatively, little effort use the verification tools provided by CPN-Tools to investigate the model and uncover potential problems and shortcomings that might have been missed during the design process.

\end{document}